// One-------------------------
// удаляет из объекта ключ с маленьким значением

const a = {
    b: 1,
    c: 3,
    z: 2,
}

let min = "z";
let minNum = a[min];
for (key in a) {
    if (minNum > a[key]) {
        minNum = a[key];
        min = key;
    }
}
delete a[min];
console.log(a)

// Two-------------------------
// копия объекта а сновым полем

const b = { ...a};
b.f = {
    h: "hi",
    j: 123,
   }

console.log(b);

// Three-------------------------
// замена элемента

b.f.j = minNum;
console.log(b)

// Four-------------------------
// есть ли элемент 'b' в объекте

"b" in a ? console.log("да"): console.log("нет");

// Five-------------------------
// изменить элемент в объекте

let aa = {
    b: 5,
}
aa = { ...aa, b: 78}
console.log(aa);
