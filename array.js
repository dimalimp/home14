let arr = [1, 3, 23, "mama", { a: 1, b: "2"}];
console.log(arr)

// Two---------------------------
// записывает последнее значение

const arrObj = arr[arr.length -1];
console.log(arrObj)

// Three---------------------------
// меняет местами первое и второе значение

const swap = arr[0];
arr[0] = arr[1];
arr[1] = swap;
console.log(arr)

// Four---------------------------
// склейка двух массивов

let arr2 = ["ек", 82];
arr = [ ...arr, ...arr2];
console.log(arr)

// Five---------------------------
// увеличивает только числовые значения на 2

for (key in arr){
  if (typeof arr[key] === "number") {
    arr[key] = arr[key] + 2;
  }
}
console.log(arr)

// Six---------------------------

let arrNumbers = [0, 56, 0, 0, 2, 15, 3, 0, 5];

// Seven---------------------------
// меняет самое первое мальенкое значение на 99

let minKey = 0;

for (key in arrNumbers) {
  if (arrNumbers[minKey] > arrNumbers[key]) {
    minKey = key;
  }
}

arrNumbers[minKey] = 99;
console.log(arrNumbers);

// Eight---------------------------
// удаляет все нули и следующии за ним

let i=0;
while (i < arrNumbers.length) {
    for (key in arrNumbers) {
        if (arrNumbers[i] === 0) {
            if (arrNumbers[i + 1] !== 0) {
                arrNumbers.splice(i, 2);
            }
            else {
                arrNumbers.splice(i, 1);
            }
        }
    }
    i = i + 1;
}
console.log(arrNumbers);


// Ten------------------------------
// получает массив из чисел которые расположены между самым первым из самых малых чисел и самым последним из самых больших


const arrNumbers = [2,1,1,9,15,11,15,10, 12];
let maxIndex = 0;
let max = arrNumbers[maxIndex];
let minIndex = 0;
let min = arrNumbers[minIndex];

for (key in arrNumbers) { // за 1 цикл можно найти сразу и мин и макс
  if (max <= arrNumbers[key]) { // именно из-за того что тут <= а не < мы точно найдем именно самое последнее из самых больших
    max = arrNumbers[key]; // оно нам даже и не нужно, но ради примера
    maxIndex = Number(key);
  }
  if (min > arrNumbers[key]) {
    min = arrNumbers[key];// оно нам даже и не нужно, но ради примера
    minIndex = Number(key);
  }
}

maxIndex > minIndex ? console.log(arrNumbers.slice(minIndex, maxIndex + 1)) : console.log(arrNumbers.slice(maxIndex, minIndex + 1))